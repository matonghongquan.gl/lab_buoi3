﻿using BUOI3.Models;
using BUOI3.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel;

namespace BUOI3.Controllers
{
    
    [Authorize(Roles = SD.Role_Admin)]
    public class ProductController : Controller
    {
        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;

        public ProductController(IProductRepository productRepository, ICategoryRepository categoryRepository)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
        }
        //hien thi ten san pham
        public async Task<IActionResult> Index()
        {
            var products=await _productRepository.GetAllAsync();
            return View(products);
        }
        //hien thi form them san pham moi
        [Authorize(Roles = SD.Role_Admin)]
        public async Task<IActionResult> Add()
        {
            var categories = await _categoryRepository.GetAllAsync();
            ViewBag.Categories = new SelectList(categories, "Id", "Name");

            return View();
        }
        //xu ly them san pham moi
        [HttpPost]
        public async Task<IActionResult> Add(Product product, IFormFile imageUrl)
        {
            if (ModelState.IsValid)
            {
                if (imageUrl!= null)
                {
                    //Luu hinh anh dai dien tham khao bai 02 ham SaveImage
                    product.ImageUrl = await SaveImage(imageUrl);
                }
                await _productRepository.AddAsync(product);
                return RedirectToAction(nameof(Index));
            }
            //Neu ModelState khong hop le, hien thi form voi du lieu da nhap
            var categories= await _categoryRepository.GetAllAsync();
            ViewBag.Category = new SelectList(categories, "Id", "Name");
            return View(product);
        }
        //Viet them ham SaveImage(Tham khao bai 02)
        private async Task<string> SaveImage(IFormFile image)
        {
            var savePath = Path.Combine("wwwroot/images", image.FileName);
            using (var fileStream = new FileStream(savePath, FileMode.Create))
            {
                await image.CopyToAsync(fileStream);
            }
            return "/images/" + image.FileName;
        }
        //hien thi thong tin san pham
        [Authorize(Roles = SD.Role_Admin)]
        public async Task<IActionResult> Display(int id)
        {
            var product = await _productRepository.GetByIdAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }
        //Hien thi form cap nhap san pham
        [Authorize(Roles = SD.Role_Admin)]
        public async Task<IActionResult> Update(int id)
        {
            var product=await _productRepository.GetByIdAsync(id);
            if(product == null)
            {
                return NotFound();
            }
            var categories=await _categoryRepository.GetAllAsync();
            ViewBag.Categories = new SelectList(categories, "Id", "Name", product.CategoryId);
            return View(product);
        }
        //xu ly cap nhap san pham
        [HttpPost]
        public async Task<IActionResult>  Update(int id,Product product, IFormFile imageUrl)
        {
            ModelState.Remove("ImageUrl");
            if (id != product.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                var exstingProduct = await _productRepository.GetByIdAsync(id);
                if (imageUrl == null)
                {
                    product.ImageUrl=exstingProduct.ImageUrl;
                }
                else
                {
                    product.ImageUrl = await SaveImage(imageUrl);
                }
                //cap nhap cac thong tin khac cua san pham
                exstingProduct.Name=product.Name;
                exstingProduct.Price=product.Price;
                exstingProduct.Description=product.Description;
                exstingProduct.CategoryId=product.CategoryId;
                exstingProduct.ImageUrl=product.ImageUrl;

                await _productRepository.UpdateAsync(exstingProduct);
                return RedirectToAction(nameof(Index));
            }
            var categories=await _categoryRepository.GetAllAsync();
            ViewBag.Categories = new SelectList(categories,"Id","Name");
            return View(categories);
        }
        //Hien thi form xac nhan san pham
        [Authorize(Roles = SD.Role_Admin)]
        public async Task<IActionResult> Delete(int id)
        {
            var product = await _productRepository.GetByIdAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }
        //xu ly xoa san pham
        [HttpPost,ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _productRepository.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
