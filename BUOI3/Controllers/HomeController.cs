using BUOI3.Models;
using BUOI3.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace BUOI3.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

       
        private readonly IProductRepository _productRepository;
        public HomeController(IProductRepository productRepository) 
        {
            _productRepository = productRepository;
        }
        //hien thi danh sach san pham
        public async Task<IActionResult> Index()
        {
            var products = await _productRepository.GetAllAsync();
            return View(products);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
